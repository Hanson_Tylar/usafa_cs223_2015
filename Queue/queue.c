#include "queue.h"

/*typedef struct queuenode {
	int number;
	struct queueNode *next;
} *QueueNodePtr;

typedef struct queue {
	QueueNodePtr beginning;
	QueueNodePtr end;
} *QueuePtr; */

// Create a queue
QueuePtr initialize() {
	QueuePtr result;
	result = malloc(sizeof(struct queue));
	result->beginning = NULL;
	result->end = NULL;
	return result;
}

// Get in line
void enqueue(QueuePtr q, int item) {
	// Nobody is in line
	if( q->beginning==NULL) {
		QueueNodePtr newOne = malloc(sizeof(struct queuenode));
		newOne->number = item;
		newOne->next=NULL;
		q->beginning = newOne;
		q->end = newOne;
	}
	else {
		QueueNodePtr newOne = malloc(sizeof(struct queuenode));
		newOne->number = item;
		newOne->next=NULL;
		q->end->next = newOne;
		q->end = newOne;
	}
}

// return 1 if q is empty, 0 otherwise
int isEmpty(QueuePtr q) {
	return(q->beginning==NULL);
}

// Remove from the line 
int dequeue(QueuePtr q) {
	if(q->beginning==NULL) {
		return -1;
	}
	else if(q->beginning->==NULL) {
		int result = q->beginning->number;
		free(q->beginning);
		q->beginning = NULL;
		q->end = NULL;
		return result;
	}
	else {
		int result = q->beginning->number;
		QueueNodePtr second = q->beginning->next;
		free(q->beginning);
		q->beginning=second;
		return result;
	}
}

void freeQueue(QueuePtr q) {
	int result;
	if(isEmpty(q)) {
		free(q);
	}
	else {
		result = dequeue(q);
		freeQueue(q);
	}
}