typedef struct queuenode {
	int number;
	struct queueNode *next;
} *QueueNodePtr;

typedef struct queue {
	QueueNodePtr beginning;
	QueueNodePtr end;
} *QueuePtr;

// Create a queue
QueuePtr initialize();

// Get in line
void enqueue(QueuePtr q, int item);

// Remove from the line 
int dequeue(QueuePtr q);

// return 1 if the queue is empty
int isEmpty(QueuePtr q);

// free all the things
void freeQueue(QueuePtr q);