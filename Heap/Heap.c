#include "Heap.h"
#include <stdlib.h>
/*typedef struct heap {
	int *things;
	int howBigIsArray;
	int lastSpotUsed;
} Heap, *HeapPtr; */

// malloc a heap and return it with an array of size "size"
HeapPtr createHeap(int size) {
	HeapPtr heap = malloc(sizeof(struct heap));
	heap->things = malloc(size*sizeof(int));
	heap->howBigIsArray = size;
	heap->lastSpotUsed = -1;
	return heap;
}

// swap the things at loc1 and loc2
void swap(int *things, int loc1, int loc2) {
	int temp=things[loc1];
	things[loc1]=things[loc2];
	things[loc2]=temp;	
}

// let the newly inserted item fight its way toward the top
void bubbleUp(int *things, int location) {
	if (location==0) {
		return;
	}
	else if (things[location]<things[(location-1)/2]) {
		return; // the man is keeping me down
	}
	else {
		// swap!
		swap(things,location,(location-1)/2);
		bubbleUp(things,(location-1)/2);
	}
}

// insertIntoHeap adds thing to the heap
// returns 1 if succeeded, and 0 if the heap was full
int insertIntoHeap(HeapPtr heap, int thing) {
	if (heap->lastSpotUsed==heap->howBigIsArray-1) {
		return 0; // fail!  we were full
	}
	heap->lastSpotUsed++;
	heap->things[heap->lastSpotUsed] = thing;
	bubbleUp(heap->things,heap->lastSpotUsed);
	return 1;
}

// go down to where you belong
void bubbleDown(int *things, int location, int end) {
	if (location*2+1>end) {
		return; // we're on the bottom row
	}
	else if (location*2+2<=end) { // we have a right kid
		if (things[location*2+2]>things[location*2+1]) {
			// right kid is bigger
			if (things[location]<things[location*2+2]){ 
				// smaller than our bigger right kid
				swap(things,location,location*2+2);
				bubbleDown(things,location*2+2,end);
			}
		}
		else 
		{
			// left kid is bigger
			if (things[location]<things[location*2+1]){ 
				// smaller than our bigger left kid
				swap(things,location,location*2+1);
				bubbleDown(things,location*2+1,end);
			}
		}
	}
	else if (things[location]<things[location*2+1]){ 
		// smaller than our one and only child
		swap(things,location,location*2+1);	
		bubbleDown(things,location*2+1,end);
	}
	// don't need anything here b/c I am bigger than my kids (for now .....)
}

// removeFromHeap sets thing to the thing removed
// thing should be a pointer to an integer variable,
// which will hold the value returned from the heap
// returns 1 if succeeded, and 0 if the heap was empty
int removeFromHeap(HeapPtr heap, int *thing) {
	if (heap->lastSpotUsed<0) {
		return 0; // fail! the heap was empty
	}
	*thing = heap->things[0];
	heap->things[0] = heap->things[heap->lastSpotUsed--];
	// heap->things[0] = heap->things[heap->lastSpotUsed];
	// heap->lastSpotUsed = heap->lastSpotUsed - 1;
    bubbleDown(heap->things,0,heap->lastSpotUsed);
    return 1;	
}