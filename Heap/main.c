#include <stdio.h>
#include "Heap.h"

int main(int argc, char **argv)
{
	printf("hello world\n");
	HeapPtr heap = createHeap(100);
	insertIntoHeap(heap,9);
	insertIntoHeap(heap,19);
	insertIntoHeap(heap,3);
	insertIntoHeap(heap,39);
	insertIntoHeap(heap,2);
	insertIntoHeap(heap,4);
	insertIntoHeap(heap,8);
	insertIntoHeap(heap,2018);
	insertIntoHeap(heap,2019);
	insertIntoHeap(heap,37);
	insertIntoHeap(heap,49);
	insertIntoHeap(heap,12);
	int result;
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	removeFromHeap(heap,&result);
	printf("Removed %d\n",result);
	return 0;
}
