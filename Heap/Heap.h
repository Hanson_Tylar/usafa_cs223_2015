typedef struct heap {
	int *things;
	int howBigIsArray;
	int lastSpotUsed;
} Heap, *HeapPtr;

// malloc a heap and return it with an array of size "size"
HeapPtr createHeap(int size);

// insertIntoHeap adds thing to the heap
// returns 1 if succeeded, and 0 if the heap was full
int insertIntoHeap(HeapPtr heap, int thing);

// removeFromHeap sets thing to the thing removed
// thing should be a pointer to an integer variable,
// which will hold the value returned from the heap
// returns 1 if succeeded, and 0 if the heap was empty
int removeFromHeap(HeapPtr heap, int *thing);