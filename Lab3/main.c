#include <stdio.h>

int exercise2() {
	printf("Enter number of minutes: ");
	float minutes;
	scanf("%f", &minutes);
	printf("User input %0.0f minutes\n", minutes);
	printf("Convert %0.0f minutes to hours(h) or days(d)?", minutes);
	char hours_or_days[2];
	// Don't use '&' to pass a string to scanf
	scanf("%1s", hours_or_days);
	printf("Convert %0.0f minutes to %s.\n", minutes, hours_or_days);
	// If string is equal to 'h' 
	if( !strcmp(hours_or_days, "h") ) {
		float hours;
		hours = 60 / minutes;
		printf("%0.0f minutes is %3.2f hours", minutes, hours);
	}
	else {
		float days;
		days = minutes / ( 24 * 60 );
		printf("%0.0f minutes is %3.3f days", minutes, days);
	}
}

int exercise3() {
	printf("Enter a numerical grade: ");
	float grade;
	scanf("%f", &grade);
	printf("%0.2f", grade);
	if( grade >= 90) {
		printf("\nYour grade is an A");
	}
	else if( grade >= 80) {
		printf("\nYour grade is a B");
	}
	else if( grade >= 70) {
		printf("\nYour grade is a C");
	}
	else if( grade >= 60) {
		printf("\nYour grade is a D");
	}
	else {
		printf("\nYour grade is an F");
	}
}

int exercise4() {
	printf("Input a temperature in Fahrenheit: ");
	float temp;
	float new_temp;
	scanf("%f", &temp);
	printf("\nConvert to: \n1) Celsuis \n2) Rankine \n3) Kelvin\n\n");
	int convert_to;
	scanf("%d", &convert_to);
	// Can use switch instead of else if statements.
	switch( convert_to ) {
		case 1 :
			new_temp = ( temp - 32 ) * 5.0 / 9.0;
			printf("\n%4.2f Fahrenheit is %4.2f Celsius", temp, new_temp); 
			break;
		case 2 :
			new_temp = temp + 459.67;
			printf("\n%4.2f Fahrenheit is %4.2f Rankine", temp, new_temp); 
			break;
		case 3 :
			new_temp = ( temp + 459.67 ) * 5.0 / 9.0;
			printf("\n%4.2f Fahrenheit is %4.2f Kelvin", temp, new_temp); 
			break;
	}
}
int main(int argc, char **argv)
{
	exercise2();
	exercise3();
	exercise4();
	return 0;
}