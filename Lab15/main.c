#include <stdio.h>
#include <stdlib.h>
// Lab 15

typedef struct node {
	int num;
	struct node *next;
} Node, *NodePtr;
	
NodePtr makeNode( int n ) {
	NodePtr np = ( NodePtr ) malloc( sizeof( Node ) );
	np -> num = n;
	np -> next = NULL;
	return np;
} // end makeNode

void printList( NodePtr np ) {
	printf("\n");
	while( np != NULL ) { // as long as there is a node
		printf( "%d\n", np -> num );
		np = np -> next; // go on to the next node
	}
} // end printList
	
NodePtr create_linked_list() {
	// Function to create a linked list
	printf( "Create a linked list.\n\n" );
	int n;
	NodePtr top, np, last;
	
	top = NULL;
	if( scanf( "%d", &n ) !=1 ) {
		n = 0;
	}
	while( n!=0 ) {
		np = makeNode( n ); // Create a new node containing n
		if( top == NULL ) {
			top = np; // set top if first node
		}
		else {
			last -> next = np; // set last -> next for other nodes
		}
		last = np; // update last to new node
		if( scanf( "%d", &n ) != 1 )  {
			n = 0;
		}
	}
	return top;
} // end create linked list

int exercise3_1( NodePtr top ) {
	// This program returns 1 if the given list is in ascending order and 0 if it is not.
	printf( "\nExercise 3.1\n");
	while( top->next != NULL ) { // check until next number is NULL
		if( top -> num > ((top -> next) -> num) ) {
			return 0;
		}
		top = top -> next; // go on to the next node
	}
	return 1;
} // end exercise3_1

void exercise3_4( NodePtr head ) {
	// A function to free all the nodes of a given linked list
	printf("\nExercise3.4\n");
	NodePtr temp;
	while( head!=NULL ) {
		temp = head;
		head = head -> next;
		free( temp );
	}
	printf("The nodes are all free.\n");
}

int main(int argc, char **argv) {
	NodePtr top = create_linked_list();
	// This function adds nodes to the end of the linked list.
	if( exercise3_1( top ) == 1 ) {
		printf("List is in ascending order.\n");
	}
	else {
		printf("List is not in ascending order.\n");
	}
	exercise3_4( top );
	return 0;
}
