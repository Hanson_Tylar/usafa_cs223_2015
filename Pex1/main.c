#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <time.h>

struct Time {
	int tm_mon;
	int tm_year;
};
struct Time t;

struct Card {
	int exp_month;
	int exp_year;
	char *network;
	char *account_number;
};
struct Card card;
int damms_alg( char number[16] ) {
	// Method to validate a credit card using Damm's Algorithm.
	// Return 1 for valid, 0 for invalid.
	int alg_table[10][10] = {
		{ 0, 3, 1, 7, 5, 9, 8, 6, 4, 2 },
		{ 7, 0, 9, 2, 1, 5, 4, 8, 6, 3, },
		{ 4, 2, 0, 6, 8, 7, 1, 3, 5, 9 },
		{ 1, 7, 5, 0, 9, 8, 3, 4, 2, 6 },
		{ 6, 1, 2, 3, 0, 4, 5, 9, 7, 8 },
		{ 3, 6, 7, 4, 2, 0, 9, 5, 8, 1 },
		{ 5, 8, 6, 9, 7, 2, 0, 1, 3, 4 },
		{ 8, 9, 4, 5, 3, 6, 2, 0, 1, 7 },
		{ 9, 4, 3, 8, 6, 1, 7, 2, 0, 9 },
		{ 2, 5, 8, 1, 4, 3, 6, 7, 9, 0 }
	};
	int row = 0;
	int i = 0;
	int col;
	for( i; i < 15 ; i++ ) {
		col = number[i] -'0';
		row = alg_table[ row ][ col ];
	}
	if( row == 0 ) {
		return 1;
	}
	else {
		return 0;
	}
}
int mod_10( char number[18] ) {
	// Method to validate a credit card with Luhn's Mod 10 algorithm.
	// Return 1 for valid 0 for invalid
	int result;
	//Sum the even digits.
	int sum_evens=0;
	int sum_odds=0;
	int length = strlen(number);
	int doubled_digit;
	// Sum even and odd digits
	for( int i=1; i<length+1; i++ ) {
		// If even, double the digit.
		if( i % 2 == 0 ) {
			doubled_digit = (number[length-i] - '0') * 2;
			// If doubled digit is greater than 9, sum its two digits
			if( doubled_digit > 9 ) {
				doubled_digit = doubled_digit % 10 + doubled_digit / 10;
				sum_evens += doubled_digit;
			}
			else {
				sum_evens += doubled_digit;
			}
		}
		else { 
			sum_odds += number[length-i] - '0';
		}
	}
	if( (sum_odds + sum_evens ) % 10 == 0 ) { 
		result = 1;
	}
	else {
		result = 0;
	}
	return result;
}
void validate_card( struct Card card ) {
	// This method checks the name of each card, it's expiration, and card number length.
	FILE *output_list;
	output_list = fopen("Validated_List.txt", "w+");
	// Check expiration date.
	// Was unable to figure out why I can't write to a file using fprint( output_list, "text" ) from within an if statement
	if( card.exp_year < t.tm_year || (card.exp_year == t.tm_year && card.exp_month < t.tm_mon)) {
		printf( "%.2d/%d %s %s INVALID EXPIRED\n", card.exp_month, card.exp_year, card.network, card.account_number );
	}
	// If Mastercard, check account number length, prefix, and check digit.
	else if( strcmp(card.network, "MASTERCARD" ) ==0 ) {
		if( strlen(card.account_number) == 16 ) {
			if( mod_10( card.account_number ) == 0 ) {
				printf( "%d/%d %s %s INVALID CHECK DIGIT\n", card.exp_month, card.exp_year, card.network, card.account_number );
			}
			else {
				printf( "%d/%d %s %s VALID\n", card.exp_month, card.exp_year, card.network, card.account_number );
			}
		}
		else {
			printf( "%d/%d %s %s INVALID CARD NUMBER\n", card.exp_month, card.exp_year, card.network, card.account_number );
		}
	}
	// If Visa check account number length, prefix, and check digit.
	else if( strcmp( card.network, "VISA" ) == 0 ) {
		if( strlen(card.account_number) == 16 || strlen(card.account_number) == 13 ) {
			if( card.account_number[0] - '0' == 4 ) {
					if( mod_10( card.account_number ) == 0 ) {
						printf( "%d/%d %s %s INVALID CHECK DIGIT\n", card.exp_month, card.exp_year, card.network, card.account_number );
					}
					else {
						printf( "%d/%d %s %s VALID\n", card.exp_month, card.exp_year, card.network, card.account_number );
					}
			}
			else {
				printf("%d/%d %s %s INVALID PREFIX\n", card.exp_month, card.exp_year, card.network, card.account_number);
			}
		}
		else {
			printf( "%d/%d %s %s INVALID NUMBER LENGTH\n", card.exp_month, card.exp_year, card.network, card.account_number );
		}
	}
	// If Amex check account number length, prefix, and check digit.
	else if( strcmp(card.network, "AMEX" ) ==0 ) {
		if( strlen(card.account_number) == 15 ) {
			if( card.account_number[0] - '0' == 3 && ( card.account_number[1] - '0' == 4 || card.account_number - '0' == 7 ) ) {
				if( mod_10( card.account_number ) == 0 ) {
					printf( "%d/%d %s %s INVALID CHECK DIGIT\n", card.exp_month, card.exp_year, card.network, card.account_number );
				}
				else {
					printf( "%d/%d %s %s VALID\n", card.exp_month, card.exp_year, card.network, card.account_number );
				}
			}
			else {
				printf("%d/%d %s %s INVALID PREFIX\n", card.exp_month, card.exp_year, card.network, card.account_number);
			}
		}
		else {
			printf( "%d/%d %s %s INVALID NUMBER LENGTH\n", card.exp_month, card.exp_year, card.network, card.account_number );
		}
	}
	// If Discover check account number length, prefix, and check digit.
	else if( strcmp(card.network, "DISCOVER" ) ==0 ) {
		if( strlen(card.account_number) == 16 ) {
			if( card.account_number[0] -'0' == 6 && card.account_number[1] -'0' == 0 && card.account_number[2] -'0' == 1 && card.account_number[3] -'0' == 1) {
				if( mod_10( card.account_number ) == 0 ) {
					printf( "%d/%d %s %s INVALID CHECK DIGIT\n", card.exp_month, card.exp_year, card.network, card.account_number );
				}
				else {
					printf( "%d/%d %s %s VALID\n", card.exp_month, card.exp_year, card.network, card.account_number );
				}
			}
			else {
				printf("%d/%d %s %s INVALID PREFIX\n", card.exp_month, card.exp_year, card.network, card.account_number );
			}
		}
		else {
			printf( "%d/%d %s %s INVALID NUMBER LENGTH\n", card.exp_month, card.exp_year, card.network, card.account_number );
		}
	}
	// If JCB check account number length, prefix, and check digit.
	else if( strcmp(card.network, "JCB" ) ==0 ) {
		if( strlen(card.account_number) == 15 ) {
			if( (card.account_number[0] -'0' == 2 && card.account_number[1] -'0' == 1 && card.account_number[2] -'0' == 3 && card.account_number[3] -'0' == 1) || ( card.account_number[0] -'0' == 1 && card.account_number[1] -'0' == 8 && card.account_number[2] -'0' == 0 && card.account_number[3] -'0' == 0 )) {
				if( damms_alg( card.account_number ) == 0 ) {
					printf( "%d/%d %s %s INVALID CHECK DIGIT\n", card.exp_month, card.exp_year, card.network, card.account_number );
				}
				else {
					printf( "%d/%d %s %s VALID\n", card.exp_month, card.exp_year, card.network, card.account_number );
				}
			}
			else {
				printf("%d/%d %s %s INVALID PREFIX\n", card.exp_month, card.exp_year, card.network, card.account_number );
			}
			}
		else {
			printf( "%d/%d %s %s INVALID NUMBER LENGTH\n", card.exp_month, card.exp_year, card.network, card.account_number );
		}
	}
	else { 
		printf ("Card network name not recognized.\n");
	}
	fprintf( output_list, "SOME TESTASDGASDG%dSDG", 4 );
	fclose( output_list );
}
void read_file(char *filename) {
	// This method opens the list of credit cards and seperates the information
	char line[100];
	char *word;
	FILE *card_list= fopen(filename,"r");
	// Read all lines until EOF.
	while( fgets(line,100, card_list) !=NULL ) {;
		// Split the line up by spaces and slashes
		word = strtok(line," /");
		card.exp_month = atoi( word );
		word = strtok (NULL, " /");
		card.exp_year = atoi( word );
		word = strtok (NULL, " /");
		// Change network name to all caps.
		for( int i =0; i< strlen(word); i++ ) { 
			word[i] = toupper(word[i]);
		}
		card.network = word ;
		word = strtok (NULL, "");
		// If account number has spaces or hyphens remove them.
		if( strchr( word, '-' ) != NULL || strchr( word, ' ' ) != NULL ) {
			int j=0;
			char number[strlen( word ) -3 ];
			for( int i=0; i < strlen( word ); i++ ) {
				if( word[i] >= '0' && word[i] <= '9' ) {
					number[i-j] = word[i];
				}
				else {
					j++;
				}
			}
			number[strlen(number)-1] = '\0';
			card.account_number = number;
		}
		else {
			card.account_number = word;
		}
		//If number ends in a newline character change it to null. Will change all but last line in file.
		if( word[strlen(word) - 1] == '\n' ) {
			word[strlen(word) - 1] = '\0';
		}
		validate_card( card );
	}
	fclose( card_list );
}
void current_time(){
	// Get the current month and year and store it in a structure.
    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
	t.tm_mon = timeinfo->tm_mon + 1;
	t.tm_year = timeinfo->tm_year + 1900 - 2000;
}
int main(int argc, char **argv) {
	printf("C3C Tylar Hanson - Pex1\nDocumentation Statement: I found code similar to my current_time function on stackexchange.com.\n" );
	printf("I changed variable names and how the data was stored to work with my program. I consulted stackexchange and tutorialspoint\n");
	printf(" for information about functions and examples.");
	current_time();
//	read_file( "list.txt" );	
	read_file(argv[1]);
	return 0;}