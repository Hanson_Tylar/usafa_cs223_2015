#include <stdlib.h>
#include <stdio.h>

// Lab 21 
// Quick Sort and Merge Sort
// Merge sort O(n*log2(n))
// Quick sort O(n*log2(n)) on average, requires less memory than merge sort

// Quick Sort
void swap(int list[], int i, int j) {
// Swap list[i] and list[j]
	int hold = list[i];
	list[i] = list[j];
	list[j] = hold;
}

int partition(int A[], int lo, int hi) {
// Partition A[lo] to A[hi] using A[lo] as the pivot
	int pivot = A[lo];
	int lastSmall = lo, h;
	for(h=lo+1; h<=hi; h++) {
		if(A[h] < pivot) {
			++lastSmall;
			swap(A, lastSmall, h);
		}
	}
	swap(A, lo, lastSmall);
	return lastSmall;
}

void quicksort(int A[], int lo, int hi) {
// Sorts A[lo] to A[hi] in ascending order
	if(lo < hi) {
		int dp = partition(A, lo, hi);
		quicksort(A, lo, dp-1);
		quicksort(A, dp+1, hi);
	}
}
int main(int argc, char **argv) {
	int n = 20;
	int A[] = { 41, 83, 38, 62, 99, 82, 6, 63, 14, 70, 56, 64, 73, 43, 91, 62, 78, 15, 31, 53 };
	for(int i=0; i<n; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
	quicksort(A, 0, n-1);
	for(int i=0; i<n; i++) {
		printf("%d ", A[i]);
	}
	return 0;
}
