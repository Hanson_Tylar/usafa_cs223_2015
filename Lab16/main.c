#include <stdio.h>
#include <stdlib.h>

// LAB 16

typedef struct node {
	int num;
	struct node *next;
} Node, *NodePtr;

NodePtr makeNode( int n ) {
	NodePtr np = (NodePtr) malloc( sizeof( Node ) );
	np -> num = n;
	np -> next = NULL;
	return np;
}

void printList( NodePtr np ) {
	while( np != NULL ) {
		printf( "%d\n", np -> num );
		np = np -> next;
	}
}
void program3_2() {
	int n;
	NodePtr top, np;
	top = NULL;
	if( scanf( "%d", &n ) !=1 ) {
		n = 0;
	}
	while( n!=0 ) {
		np = makeNode( n );
		np -> next = top;
		top = np;
		if( scanf( "%d", &n ) !=1 ) {
			n = 0;
		}
	}
	printList( top );
}
int main(int argc, char **argv){
	program3_2();
	// Program 3.2 add a new item at the head of a linked list
//	exercise3_5();
	return 0;
}
