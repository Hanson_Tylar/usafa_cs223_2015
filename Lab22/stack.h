typeef struct stacknode {
	int number;
	struct stacknode *down;
} StackNode, *StackNodePtr;

typedef struck aStack {
	StackNodePtr top;
} *Stack;

Stack initialize(Stack *stack );
//Create an empty stack

void push(Stack stack, int item);
//Place and item on the top of the Stack

int pop(Stack stack);
//Take the top item off the Stack
//Don't call pop, when the stack is empty, or else

int isEmpty(Stack stack);
//Is the stack empty (has zero lines)

void freeStack(Stack stack);
//Free all the memory in the stack