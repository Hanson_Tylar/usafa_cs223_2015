#include "stack.h"

//typeef struct stacknode {
//	int number;
//	struct stacknode *down;
//} StackNode, *StackNodePtr;
//
//typedef struck aStack {
//	StackNodePtr top;
//} *Stack;

//Create an empty stack
Stack initialize() {
	Stack answer = malloc(sizeof(struct aStack));
	answer->top = NULL;
	return answer;
}

//Place and item on the top of the Stack
void push(Stack stack, int item) {
	StackNodePtr newOne = malloc(sizeof(StackNode));
	newOne->down = stack->top;
	newOne->number = item;
	stack->top = newOne;
}

//Take the top item off the Stack
int pop(Stack stack) {
//	if(isEmpty(stack)) {
//		return -1;
//	}
	int result = stack->top->number;
	StackNodePtr temp = stack->top;
	stack->top=temp->down;
	free(temp);
	return result;
}

//Is the stack empty (has zero lines)
int isEmpty(Stack stack) {
	if(stack->top==NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

void freeAllTheThings(StackNodePtr s) {
	if(s==NULL) {
		return;
	}
	else {
		freeAllTheThings(s->down);
		free(s);
	}
}

//Free all the memory in the stack
void freeStack(Stack stack) {
	freeAllTheThings(stack->top);
}
