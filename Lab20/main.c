#include <stdio.h>
#include <stdlib.h>
#define MAXNUMBERS 50000
// Lab 20 Sorting algorithms

// selection sort is O(n^2) 
// Bubble sort is also O(n^2)
// insertion sort is O(n^2)
// log.2(n) = how many times can I cut n in half before I get one.

void insertionSort( int list[], int n ) {
	for ( int h=1; h<n; h++ ) {
		int key = list[h];
		int k = h-1; //Start comparing with previous item
		while( k >=0 && key<list[k] ) {
			list[k+1]=list[k];
			--k;
		}
		list[k-1]=key;
	}
}
 
int main(int argc, char **argv)
{
	int *list1 = malloc(sizeof(int)*MAXNUMBERS);
	for( int i=0; i<MAXNUMBERS; i++) {
		list1[i] = rand()%100000;
	}
	insertionSort( list1, MAXNUMBERS );
	return 0;
}
