#include <stdio.h>
#include <ctype.h>

// LAB 13 RECURSION

int factorial( int n ) {
	//Recursively calculates the factorial of a given number.
	if( n <= 1 ) {
		return 1;
	}
	else {
		return n * factorial( n - 1 );
	}
}

int factorial_verbose( int n ) {
	//Recursively calculates the factorial of a given number.
	printf( " n = %d: ", n );
	if( n <= 1 ) {
		printf( "Returning 1\n" );
		return 1;
	}
	else {
		printf( "Calculating result = %d * factorial( %d )\n", n, n-1 );
		int result = n * factorial_verbose( n - 1 );
		printf( "n = %d: returning %d", n, result );
		return result;
	}
}
void exercise1() {
	printf("Exercise 1\n\n");
	printf("Factorial of 5 = %d\n", factorial(4));
	printf("Factorial of 5 = %d\n", factorial_verbose( 5 ) );
}

int fibonacci( int n ) {
	//Recursively calculates the nth Fibonacci number.
	if( n <= 1 ) {
		return n;
	}
	else {
		int result = fibonacci( n - 1 ) + fibonacci( n - 2 );
		return result;
	}
}

void exercise2() {
	printf("\n\nExercise 2.\n\n");
	printf("The %d Fibonacci number is %d.\n", 5, fibonacci( 5 ) );
}

int gcd( int a, int b ) {
	//Recursively calculates the GCD of two given numbers.
	if( b == 0 ){
		return a;
	}
	else {
		return gcd( b, a % b );
	}
}

int mult( int a, int b ) {
	if( a==0 || b==0 ) {
		return 0;
	}
	else {
		return a + mult( a, b - 1 );
	}
}
void exercise3() {
	printf("\nExercise 3\n\n");
	printf("The GCD of %d and %d is %d\n", 15, 10, gcd( 15, 10 ) );
}

int is_palindrome( int start, int end, char *string ) {
	//Recursively calculates whether or not a string is a palindrome.
	//Return 1 for True, 0 for False.
	if( string[start] != string[end] ) { 
		return 0;
	}
	else if( isalnum( string[start] ) == 0 ) {
		return is_palindrome( start+1, end, string );
	}
	else if( isalnum( string[end] ) == 0 ) {
		return is_palindrome( start, end-1, string );
	}
	else if( start >= end ) { 
		return 1;
	}
	else {
		return is_palindrome( start+1, end-1, string );
	}
}
void exercise4() {
	printf("\nExercise 4\n\n");
	char string[50]= "c";
	while( strcmp( string, "x" ) != 0 ) {
		printf("Enter a string to test: ");
		fflush( stdin );
		fgets( string, 50, stdin );
		string[strlen(string)-1]='\0';
		int start=0;
		int end=strlen(string)-1;
		if( is_palindrome( start, end, string ) == 1 ) {
			printf("\n%s is a palindrome.\n\n", string );
		}
		else {
			printf("\n%s is not a palindrome.\n\n", string );
		}
	}
}
int main(int argc, char **argv)
{
//	exercise1();
//	exercise2();
//	exercise3();
	exercise4();
	return 0;
}
