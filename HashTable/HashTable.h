#include "list.h"
#define ARRAY_SIZE 256

typedef List Hash_Table[ARRAY_SIZE];

// initialize all the pointers to NULL
void initializeHash(Hash_Table hashTable);

// add this string to the hash table
void insertItemInHash(Hash_Table hashTable, char *item);

// remove this string from the hash table
void deleteItemFromHash(Hash_Table hashTable, char *item);

// is the string in there?
int findItemInHash(Hash_Table hashTable, char *item);

// put everything away!
void freeHashTable(Hash_Table hashTable);