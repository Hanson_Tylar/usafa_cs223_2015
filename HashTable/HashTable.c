#include "HashTable.h"
#include <stdlib.h>
#include <string.h>

int hash_function(char *item) {
	int hash=0,i;
	for (i=0; i<strlen(item); i++) {
		hash += item[i];
	}
	return hash%ARRAY_SIZE;
}

// initialize all the pointers to NULL
void initializeHash(Hash_Table hashTable) {
	int i;
	for (i=0; i < ARRAY_SIZE; i++) {
		hashTable[i]=NULL;
	}
}

// add this string to the hash table
void insertItemInHash(Hash_Table hashTable, char *item) {
	int hash = hash_function(item);
	hashTable[hash]=insertItem(hashTable[hash],item);
}

// remove this string from the hash table
void deleteItemFromHash(Hash_Table hashTable, char *item) {
	int hash = hash_function(item);
	hashTable[hash]=deleteItem(hashTable[hash],item);
}

// is the string in there?
int findItemInHash(Hash_Table hashTable, char *item) {
	int hash = hash_function(item);
	return isInList(hashTable[hash],item);	
}

// put everything away!
void freeHashTable(Hash_Table hashTable) {
	int i;
	for (i=0; i<ARRAY_SIZE; i++) {
		freeList(hashTable[i]);
		hashTable[i]=NULL;
	}
}