#ifndef LIST_H
#define LIST_H
typedef struct list {
    char *data;
    struct list *next;
} *List;

// List is a pointer to a struct list.

// add item to the linked list in sorted order
// increasing order of integers
List insertItem(List head, char *item);

// look for item and make it go bye-bye
List deleteItem(List head, char *item);

// return 0 if item is NOT in the list, and 1 if it is.
int isInList(List head, char *item);

// print out all the strings in the list
void printList(List head);

// put all the list away
void freeList(List head);
#endif 