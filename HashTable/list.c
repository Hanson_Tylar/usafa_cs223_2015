#include "list.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// add item to the linked list in sorted order
// return the new beginning of the list (with what we inserted)
List insertItem(List head, char *item) {
    List temp;
    // I can solve at least one small problem
    // zero items in the list
    if (head==NULL) {
        temp = malloc(sizeof(struct list));
		// make a copy of the string when we add
        temp->data = malloc(strlen(item)+1);
		strcpy(temp->data,item);
        temp->next = NULL;
        return temp;
    }
    // inserting before the first item in the list
    else if (strcmp(item,head->data)<0) {
        temp = malloc(sizeof(struct list));
		// make a copy of the string when we add
        temp->data = malloc(strlen(item)+1);
		strcpy(temp->data,item);
        temp->next = head;
        return temp;
    }
    // If I can solve a slightly smaller problem, I can solve this one.
    else {
        temp=insertItem(head->next,item);
        head->next = temp; // why????
        // new beginning of list is old beginning of list
        return head;
    }
}



// returns the new head of the list
List deleteItem(List head, char *item) {
   // I can solve at least one small problem
   // there ain't no list, so return NULL 
   if (head==NULL) {
       return NULL;
   }    
   else if (strcmp(head->data,item)==0) {
       List temp = head->next;
	   free(head->data);
       free(head);
       return temp;
   }
   // If I can solve a slightly smaller problem, I can solve this one.
   else {
       head->next = deleteItem(head->next,item);
       return head;
   }
}



void printList(List head) {
    if (head==NULL) {
        return;
    }
    else {
        printf("%s\n",head->data);
        printList(head->next);
    }
}

// return 0 if item is NOT in the list, and 1 if it is.
int isInList(List head, char *item) {
	if (head==NULL) {
		return 0;
	}
	else if(strcmp(head->data,item)==0) {
		return 1;
	}
	else {
		return isInList(head->next,item);
	}
}

// put all the list away
void freeList(List head) {
	if (head==NULL) {
		return;
	}
	else {
		freeList(head->next);
		free(head->data);
		free(head);
	}
}
/*char plus;
int coef;
...
sscanf(s,"%c%d%c%c%d", &plus, &coef, &x, &carat, &exp);*/