#include <stdio.h>
#include "list.h" // use quotes to use your own functions

int main(int argc, char **argv)
{
	List myCoolList = NULL;
	myCoolList = insertItem( myCoolList, 10 );
	myCoolList = insertItem( myCoolList, 4 );
	myCoolList = insertItem( myCoolList, 12 );
	myCoolList = insertItem( myCoolList, 18 );
	myCoolList = insertItem( myCoolList, 19 );
	myCoolList = insertItem( myCoolList, 13 );
	
	printf("The following should be 4\n");
	printf("The 0th item is %d\n", getNthItem(myCoolList, 0));
	printf("The following should be 18\n");
	printf("The 4th item is %d\n", getNthItem(myCoolList, 4));
	
	int item = 18;
	if( isInList( myCoolList, item ) == 1 ) {
		printf("The item %d is in the list\n", item );
	}
	else {
		printf("The item %d is NOT in the list\n", item );
	}
	
	myCoolList = deleteNthItem( myCoolList, 0 );
	printf( "Deleting the 0th item...\n");
	if( isInList( myCoolList, item ) == 1 ) {
		printf("The item %d is in the list\n", 4 );
	}
	else {
		printf("The item %d is NOT in the list\n", 4 );
	}
	 myCoolList = reverse( myCoolList );
	 printList( myCoolList );
	return 0;
}
