typedef struct list {
	int data;
	struct list *next;
} *List; // List is a pointer to a struct list.

// add item to the linked list in sorted order
// increasing order of integers
List insertItem( List head, int item );

// return the nth integer from the list
int getNthItem( List head, int n );

// Delete the nth item from a list
List deleteNthItem( List head, int item );

// return 0 if the item is NOT in the list, 1 if otherwise
int isInList( List head, int item );

// put the list in reverse order
List reverse( List head );

void printList( List head );