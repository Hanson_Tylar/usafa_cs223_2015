#include "list.h"
#include <stdlib.h>

// add item to the linked list in sorted order
// return the new beginning of the list ( with what we inserted )

List insertItem( List head, int item ) {
	List temp;
	// I can solve one small problem
	// Zero items in the list
	if( head==NULL ) {
		temp = malloc(sizeof(struct list));
		temp -> data = item;
		temp -> next = NULL;
		return temp;
	}
	// Inserting before the first item in the list
	else if( item < head -> data ) {
		temp = malloc(sizeof(struct list ));
		temp -> data = item;
		temp -> next = head;
		return temp;
	}
	// If I can solve a slightly smaller problem, I can solve this one
	else {
		temp = insertItem( head -> next, item );
		head -> next = temp; // new beginning of the list is old beginning of list
		return head;
	}
} // end insertItem

// return the nth integer from the list
// n=0 will refer to head of list
int getNthItem( List head, int n ) {
	// I can solve one small problem
	if( n==0 ) {
		return head -> data;
	}
	// If I can solve a slightly smaller problem I can solve this one
	else {
		return getNthItem( head -> next, --n );
	}
} // end getNthItem

// Delete the Nth item of a list, returns new head of the list
List deleteNthItem( List head, int item ) {
	// There is no list
	if( head==NULL ) {
		return NULL;
	}
	// If item is first in the list
	else if( head -> data == item ) {
		List temp = head -> next;
		free( head );
		return temp;
	}
	else {
		head -> next = deleteNthItem( head -> next, item );
		return head;
	}
}

// return 0 if the item is NOT in the list, 1 if otherwise
int isInList( List head, int item ) {
	// There is no list
	if( head==NULL ) {
		return 0;
	}
	else if( head -> data == item ) {
		return 1;
	}
	else {
		return isInList( head -> next, item );
	}
}

List addToEnd ( List singleNode, List listOfNodes ) {
	if( listOfNodes == NULL ) {
		singleNode -> next = NULL;
		return singleNode;
	}
	else {
		listOfNodes -> next = addToEnd( singleNode, listOfNodes -> next );
		return listOfNodes;
	}
}
// Put the list in reverse order
List reverse( List head ) {
	// I can solve at least on small problem
	if( head == NULL ) {
		return NULL;
	}
	else if( head -> next == NULL ) {
		return head;
	}
	// If I can solve a slightly smaller problem I can solve this problem
	else {
		List subresult = reverse( head -> next );
		return addToEnd( head, subresult );
	}
}

void printList( List head ) {
	if( head == NULL ) {
		return;
	}
	else {
		printf( "%d\n", head -> data );
		printList( head -> next );
	}
}