#include <stdio.h>
#include <math.h>

int print_area()
{
	int r = 10;
	float area = M_PI * r * r;
	printf("Area of a circle with radius %d is %f\n", r, area);
	printf("Area of a circle with radius 0x%x is %f\n", r, area);
	printf("Area of a circle with radius %2.0d is %3.3f\n", r, area);
}
int Size_in_memory()
{		
	printf("Size in memory of int is %d\n", sizeof( int ));
	printf("Size in memory of long is %d\n", sizeof( long ));
	printf("Size in memory of char is %d\n", sizeof( char ));
	printf("Size in memory of float is %d\n", sizeof( float ));
	printf("Size in memory of double is %d\n", sizeof( double ));
}
int int_to_ascii()
{
	int number;
	printf( "Enter an integer: ");
	scanf( "%d", &number );
	printf( "Number entered in ASCII is %c \n", number );
	number ++;
	printf("Number incremented by 1 is %c\n", number );
	number = number << 1;
	/*Bit shifting left is equivalent to multiplying by 2*/
	printf( "Number bit shifted left by 1 is %d\n", number );
}
int main(int argc, char **argv)
{
	print_area();
	printf("\n");
	Size_in_memory();
	printf("\n");
	int_to_ascii();
}