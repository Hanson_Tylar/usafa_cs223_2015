#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Polynomial.h"
#include "unit_tests.h"

void run_unit_tests() {
	// Run all unit tests and store results in a text file.
	FILE *results = fopen( "results.txt", "w+" );
	if ( results == NULL ) {
		printf( "Error opening file!\n" );
		exit(1);
	}
	// Test add_poly method
	int a = test_addPoly();
	if( a == 0 ) {
		fprintf( results, "All add_poly test cases passed.\n" );
	}
	else {
		fprintf( results, "add_poly test case #%d failed.\n", a );
	}
	// Test sub_poly method
	int b = test_subPoly();
	if( b == 0 ) {
		fprintf( results, "All sub_poly test cases passed.\n" );
	}
	else {
		fprintf( results, "sub_poly test case #%d failed.\n", b );
	}
	// Test mul_poly method
	int c = test_mulPoly();
	if( c == 0 ) {
		fprintf( results, "All mul_poly test cases passed.\n" );
	}
	else {
		fprintf( results, "mul_poly test case #%d failed.\n", c );
	}
	fclose( results );
}

int main(int argc, char **argv) {
	printf("C3C Tylar Hanson\nPex2 Polynomial Calculator\nDocumentation: None\n\n");
	run_unit_tests();
	printf("Running unit tests. Results stored in /Debug/results.txt\n\n");
	polyPtr a = NULL;
	polyPtr b = NULL;
	polyPtr c = NULL;
	char p1[MAX_POLY_LENGTH]= "init";
	char p2[MAX_POLY_LENGTH] = "init";
	char p3[MAX_POLY_LENGTH];
	printf( "Enter q to quit.\n\n" );
	while( strncmp( p1, "q", 1 ) && strncmp( p1, "Q", 1 ) && strncmp( p2, "q", 1 ) && strncmp( p2, "Q", 1 )  ) {
		// Ask for polynomial 1 until valid input is given or a q / Q is given
		while( a == NULL ) {
			printf("Enter polynomial 1: ");
			fflush( stdin );
			fgets( p1, MAX_POLY_LENGTH, stdin );
			if( !strncmp( p1, "q", 1 ) || !strncmp( p1, "Q", 1 ) ) {
				return 0;
			}
			else {
				p1[strlen(p1)-1]='\0';
				a = create_poly( p1 );
//				a = create_implicit_poly( p1 );
			}
			// If a is NULL because the only term given is zero, don't ask for another poly
			if( a==NULL && ( !strncmp( p1, "+0", 2 ) || !strncmp( p1, "+-0", 3 ) ) ) {
				break;
			}
		}
		// Ask for polynomial 2 until valid input is given or a q / Q is given
		while( b == NULL ) {
			printf("Enter polynomial 2: ");
			fflush( stdin );
			fgets( p2, MAX_POLY_LENGTH, stdin );
			if( !strncmp( p2, "q", 1 ) || !strncmp( p2, "Q", 1 ) ) {
				return 0;
			}
			else {
				p2[strlen(p2)-1]='\0';
				b = create_poly( p2 );
//				b = create_implicit_poly( p2 );
			}
			// If b is NULL because the only term given is zero, don't ask for another poly
			if( b==NULL && ( !strncmp( p2, "+0", 2 ) || !strncmp( p2, "+-0", 3 ) ) ) {
				break;
			}
		}
		
		c = add_poly( a, b );
		toString( c, p3, MAX_POLY_LENGTH );
		printf("\n( %s ) + ( %s ) = %s\n\n", p1, p2, p3 );
		c = sub_poly( a, b );
		toString( c, p3, MAX_POLY_LENGTH );
		printf("( %s ) - ( %s ) = %s\n\n", p1, p2, p3 );
		c = mul_poly( a, b );
		toString( c, p3, MAX_POLY_LENGTH );
		printf("( %s ) x ( %s ) = %s\n\n", p1, p2, p3 );
		a = NULL;
		b = NULL;
	}
	return 0;
}