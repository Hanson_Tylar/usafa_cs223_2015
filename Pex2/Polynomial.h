// Documentation : None 

#define MIN_TERM_LENGTH sizeof(int)
#define MAX_TERM_LENGTH 16
#define MAX_POLY_LENGTH 128

typedef struct term {
	int coef;
	int exp;
	struct term *next;
} poly, *polyPtr;

polyPtr create_poly( char *string );
// A method that converts a string of the form 4x^3+1x^2+-3x^1+9x^0 into a polynomial.

void toString( polyPtr head, char *string, int len );
// A method that returns a string representation of the polynomial.

polyPtr add_poly( polyPtr a, polyPtr b );
// A method that adds two polynomials.

polyPtr sub_poly( polyPtr a, polyPtr b );
// A method that subtracts polynomial b from polynomial a.

polyPtr mul_poly( polyPtr a, polyPtr b );
// A method that multiplies two polynomials