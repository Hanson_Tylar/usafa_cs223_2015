##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=PEX2_Hanson
ConfigurationName      :=Debug
WorkspacePath          := "C:\Users\C18Tylar.Hanson\Documents\Spring_2016\CS_223\CodeLiteWorkspace"
ProjectPath            := "C:\Users\C18Tylar.Hanson\Documents\Spring_2016\CS_223\CodeLiteWorkspace\Pex2"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=C18Tylar.Hanson
Date                   :=29/03/2016
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/TDM-GCC-64/bin/g++.exe
SharedObjectLinkerName :=C:/TDM-GCC-64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="PEX2_Hanson.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/TDM-GCC-64/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/TDM-GCC-64/bin/ar.exe rcu
CXX      := C:/TDM-GCC-64/bin/g++.exe
CC       := C:/TDM-GCC-64/bin/gcc.exe
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := C:/TDM-GCC-64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
Objects0=$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IntermediateDirectory)/polynomial.c$(ObjectSuffix) $(IntermediateDirectory)/unit_tests.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "./Debug"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.c$(ObjectSuffix): main.c $(IntermediateDirectory)/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/C18Tylar.Hanson/Documents/Spring_2016/CS_223/CodeLiteWorkspace/Pex2/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main.c$(DependSuffix) -MM "main.c"

$(IntermediateDirectory)/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.c$(PreprocessSuffix) "main.c"

$(IntermediateDirectory)/polynomial.c$(ObjectSuffix): polynomial.c $(IntermediateDirectory)/polynomial.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/C18Tylar.Hanson/Documents/Spring_2016/CS_223/CodeLiteWorkspace/Pex2/polynomial.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/polynomial.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/polynomial.c$(DependSuffix): polynomial.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/polynomial.c$(ObjectSuffix) -MF$(IntermediateDirectory)/polynomial.c$(DependSuffix) -MM "polynomial.c"

$(IntermediateDirectory)/polynomial.c$(PreprocessSuffix): polynomial.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/polynomial.c$(PreprocessSuffix) "polynomial.c"

$(IntermediateDirectory)/unit_tests.c$(ObjectSuffix): unit_tests.c $(IntermediateDirectory)/unit_tests.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/C18Tylar.Hanson/Documents/Spring_2016/CS_223/CodeLiteWorkspace/Pex2/unit_tests.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/unit_tests.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/unit_tests.c$(DependSuffix): unit_tests.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/unit_tests.c$(ObjectSuffix) -MF$(IntermediateDirectory)/unit_tests.c$(DependSuffix) -MM "unit_tests.c"

$(IntermediateDirectory)/unit_tests.c$(PreprocessSuffix): unit_tests.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/unit_tests.c$(PreprocessSuffix) "unit_tests.c"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


