#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Polynomial.h"
#include "unit_tests.h"

// Method to simplify a given poly
// Not actually used anywhere here, but it works. 
// Ax^n+Bx^n = (A+B)x^n
polyPtr simplify_poly(polyPtr a) {
	polyPtr result=NULL;
	if(a==NULL) {
		return NULL;
	}
	else if(a->next==NULL) {
		result = malloc(sizeof(struct term));
		result->coef = a->coef;
		result->exp = a->exp;
		result->next = NULL;
		return result;
	}
	else if(a->exp==(a->next)->exp) {
		result = malloc(sizeof(struct term));
		result->coef = a->coef + a->next->coef;
		result->exp = a->exp;
		result->next = simplify_poly(a->next->next);
	}
	else {
		result = malloc(sizeof(struct term));
		result->coef = a->coef;
		result->exp = a->exp;
		result->next = simplify_poly(a->next);
	}
		return result;
}

// unfinished method that will create a poly with implicit prefix, coefficients, and exponents.
// completed create poly below that only works for explicitly defined polys
polyPtr create_implicit_poly( char *string ) {
	polyPtr result;
	if( string == NULL ) {
		return NULL;
	}
	int coef;
	int exp;
	int retval = sscanf( string, "+%dx^%d", &coef, &exp );
	// retval is 0 if no + is included in input
	if( retval == 0 ) {
		retval = sscanf( string, "%dx^%d", &coef, &exp );
		// retval is now 0 if the prefix and coefficient are implicit. 
		if( retval == 2 ) {
			result = malloc(sizeof(struct term));
			result->coef = 1;
			result->exp = exp;
			result->next = create_implicit_poly(strchr( &string[1], '+' ));
		}
		else if( retval == 0 ) { 
			retval == sscanf( string, "x^%d", &exp );
			if( retval == 1 ){
				result = malloc(sizeof(struct term));
				result->coef = 1;
				result->exp = exp;
				result->next = create_implicit_poly(strchr( &string[1], '+' ));
			}
			
		}
	}
	if( retval != 2 ) {
		printf( "Please enter a polynomial of the form +Ax^n+Bx^(n-1)+-Cx^(n-2)\nRetval = %d\n", retval);
		return NULL;
	}
	if( coef ==0 ) {
		result = create_poly( strchr( &string[1], '+' ) );
	}
	else {
		
		result = malloc(sizeof(struct term));
		result -> coef = coef;
		result -> exp = exp;	
		result -> next = create_poly( strchr( &string[1], '+' ) );
	}
	return simplify_poly(result);
}
polyPtr create_poly( char *string ) {
	polyPtr result;
	if( string == NULL ) {
		return NULL;
	}
	int coef;
	int exp;
	int retval = sscanf( string, "+%dx^%d", &coef, &exp );
	if( retval != 2 ) {
		printf( "Please enter a polynomial of the form +Ax^n+Bx^(n-1)+-Cx^(n-2)\n" );
		return NULL;
	}
	if( coef ==0 ) {
		result = create_poly( strchr( &string[1], '+' ) );
	}
	else {
		
		result = malloc(sizeof(struct term));
		result -> coef = coef;
		result -> exp = exp;	
		result -> next = create_poly( strchr( &string[1], '+' ) );
	}
	return simplify_poly(result);
}

void toStringRec( polyPtr head, char *string, int len ) {
	// Recursive helper method to convert a given poly to a string.
	if( head == NULL ) {
		return;
	}
	else {
		int sublen = snprintf( string, MAX_POLY_LENGTH, "+%dx^%d", head -> coef, head -> exp );
		toStringRec( head -> next , string + strlen( string ), len-sublen );
		return;
	}
}

void toString( polyPtr head, char *string, int len ) {
	if ( head == NULL) {
		snprintf( string, len, "0" );
	}
	else {
		toStringRec( head, string, len );
	}
	return;
}

polyPtr add_poly( polyPtr a, polyPtr b ) {
	polyPtr result;
	if( a == NULL && b == NULL ) {
		return NULL;
	}
	else if( a == NULL ) {
		result = malloc(sizeof(struct term));
		result -> coef = b -> coef;
		result -> exp = b -> exp;	
		result -> next = add_poly( a, b -> next );
	}
	else if( b == NULL ) {
		result = malloc(sizeof(struct term));
		result -> coef = a -> coef;
		result -> exp = a -> exp;	
		result -> next = add_poly( a -> next, b );
	}
	else if( a -> exp == b -> exp ) {
		if (a->coef+b->coef==0) {
			return add_poly( a -> next, b -> next );
		}
		else{
		result = malloc(sizeof(struct term));
		result -> coef = ( a -> coef ) + ( b -> coef );
		result -> exp = a -> exp;	
		result -> next = add_poly( a -> next, b -> next );
		}
	}
	else if ( a -> exp > b -> exp ) {
		result = malloc(sizeof(struct term));
		result -> coef = a -> coef;
		result -> exp = a -> exp;	
		result -> next = add_poly( a -> next, b );
	}
	else {
		result = malloc(sizeof(struct term));
		result -> coef = b -> coef;
		result -> exp = b -> exp;	
		result -> next = add_poly( a, b -> next );
	}
	return result;
}

polyPtr neg_copy_poly( polyPtr head ) {
	// multiply all terms in given poly by -1.
	polyPtr result; 
	if( head == NULL ) {
		return NULL;
	}
	else {
		result = malloc(sizeof( struct term));
		result -> coef = -1*( head -> coef );
		result -> exp = head -> exp;
		result -> next = neg_copy_poly( head -> next );
	}
	return result;
}

void free_poly( polyPtr head ) {
	// A method to free a polynomial
	polyPtr	old;
	if( head == NULL ) {
		return;
	}
	else {
		old = head;
		head = head -> next;
		free(old);
		free_poly( head );
	}
	return;
}

polyPtr sub_poly( polyPtr a, polyPtr b ) {
	polyPtr neg_b, result;
	neg_b = neg_copy_poly( b );
	result = add_poly( a, neg_b );
	free_poly( neg_b );
	return result;
}

polyPtr mul_helper( polyPtr a_head, polyPtr b ) {
	// A helper method that can multiply 1 term by n terms.
	polyPtr result;
	if( a_head == NULL || b == NULL ) {
		return NULL;
	}
	else {
		result = malloc(sizeof(struct term));
		result -> coef = a_head -> coef * b -> coef;
		result -> exp = a_head -> exp + b -> exp;
		result -> next = mul_helper( a_head, b -> next );
	}
	return result;
}

polyPtr mul_poly( polyPtr a, polyPtr b ) {
	polyPtr result;
	if( a==NULL || b==NULL ) {
		return NULL;
	}
	else {
		result = malloc(sizeof(struct term));
		result = add_poly( mul_helper( a, b ),  mul_poly( a -> next, b ) );
	}
	return result;
}