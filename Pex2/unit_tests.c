// add dif lengths
// add to zero or null
// add 3x  to -3x

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Polynomial.h"
#include "unit_tests.h"

int test_addPoly() {
	
	polyPtr a, b, c = NULL;
	char poly_c[MAX_POLY_LENGTH];
	// Zero plus zero
	a = create_poly( "+-0x^1" );
	b = create_poly( "+0x^2" );
	c = add_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "0", poly_c ) ) { return 1; }
	// Add two all positive term polys
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+3x^2+2x^1+1x^0" );
	c = add_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+6x^2+4x^1+2x^0", poly_c ) ) { return 2; }
	// Result includes one zero term
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+3x^2+2x^1+-1x^0" );
	c = add_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+6x^2+4x^1", poly_c ) ) { return 3; }
	// Result includes two zero terms
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+3x^2+-2x^1+-1x^0" );
	c = add_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+6x^2", poly_c ) ) { return 4; }
	// Add one with all positive terms to poly with all negative terms resulting in zero
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+-3x^2+-2x^1+-1x^0" );
	c = add_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "0", poly_c ) ) { return 5; }
	// Add positive term to two negative terms
	a = create_poly( "+3x^2" );
	b = create_poly( "+-2x^1+-1x^0" );
	c = add_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+3x^2+-2x^1+-1x^0", poly_c ) ) { return 6; }

	return 0;
}

int test_subPoly() {
	
	polyPtr a, b, c = NULL;
	char poly_c[MAX_POLY_LENGTH];
	
	//	pos from pos, result positive
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+2x^2+2x^1+1x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+1x^2", poly_c ) ) { return 1; }
	//	pos from pos, result zero
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+3x^2+2x^1+1x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "0", poly_c ) ) { return 2; }
	//	pos from pos, result negative
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+4x^2+3x^1+2x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+-1x^2+-1x^1+-1x^0", poly_c ) ) { return 3; }
	//	pos from neg result always neg
	a = create_poly( "+-3x^2+-2x^1+-1x^0" );
	b = create_poly( "+4x^2+3x^1+2x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+-7x^2+-5x^1+-3x^0", poly_c ) ) { return 4; }
	//	neg from pos always pos
	a = create_poly( "+3x^2+2x^1+1x^0" );
	b = create_poly( "+-4x^2+-3x^1+-2x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+7x^2+5x^1+3x^0", poly_c ) ) { return 5; }
	//	neg from neg result is positive gt, ls, zer
	a = create_poly( "+-3x^2+-2x^1+-1x^0" );
	b = create_poly( "+-4x^2+-3x^1+-2x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+1x^2+1x^1+1x^0", poly_c ) ) { return 6; }
	//	neg from neg result is zero
	a = create_poly( "+-3x^2+-2x^1+-1x^0" );
	b = create_poly( "+-3x^2+-2x^1+-1x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "0", poly_c ) ) { return 7; }
	//	neg from neg result is negative
	a = create_poly( "+-3x^2+-2x^1+-1x^0" );
	b = create_poly( "+-2x^2+-1x^1+-0x^0" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+-1x^2+-1x^1+-1x^0", poly_c ) ) { return 8; }
	//	pos from zero result always negative
	a = create_poly( "+0x^5" );
	b = create_poly( "+4x^2+3x^1" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+-4x^2+-3x^1", poly_c ) ) { return 9; }
	//	neg from zero result always pos
	a = create_poly( "+0x^5" );
	b = create_poly( "+-4x^2+-3x^1" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^2+3x^1", poly_c ) ) { return 10; }
	//	zero from neg result same
	a = create_poly( "+-4x^2+-3x^1" );
	b = create_poly( "+0x^5" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+-4x^2+-3x^1", poly_c ) ) { return 11; }
	//	zero from pos result same
	a = create_poly( "+4x^2+3x^1" );
	b = create_poly( "+0x^5" );
	c = sub_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^2+3x^1", poly_c ) ) { return 12; }
	
	return 0;
}

int test_mulPoly() {
	
	polyPtr a, b, c = NULL;
	char poly_c[MAX_POLY_LENGTH];
	
	//	Zero times Zero
	a = create_poly( "+0x^2" );
	b = create_poly( "+0x^5" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "0", poly_c ) ) { return 1; }
	//	Non zero times zero
	a = create_poly( "+5x^2" );
	b = create_poly( "+0x^5" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "0", poly_c ) ) { return 2; }
	//	Non zero times non zero
	a = create_poly( "+1x^2" );
	b = create_poly( "+4x^5" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^7", poly_c ) ) { return 3; }
	//	One term by two terms
	a = create_poly( "+1x^2" );
	b = create_poly( "+4x^5+3x^4" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^7+3x^6", poly_c ) ) { return 4; }
	//	One term by three terms
	a = create_poly( "+1x^2" );
	b = create_poly( "+4x^5+3x^4+-6x^3" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^7+3x^6+-6x^5", poly_c ) ) { return 5; }
	//	Two terms by two terms
	a = create_poly( "+1x^2+-3x^1" );
	b = create_poly( "+4x^4+3x^3" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^6+-9x^5+-9x^4", poly_c ) ) { return 6; }
	//	Two terms by three terms
	a = create_poly( "+1x^2+-3x^1" );
	b = create_poly( "+4x^5+3x^4+-5x^1" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^7+-9x^6+-9x^5+-5x^3+15x^2", poly_c ) ) { return 7; }
	//	Three by three terms
	a = create_poly( "+1x^3+-3x^2+0x^1" );
	b = create_poly( "+4x^5+3x^4+-5x^1" );
	c = mul_poly( a, b );
	toString( c, poly_c, MAX_TERM_LENGTH );
	if( strcmp( "+4x^8+-9x^7+-9x^6+-5x^4+15x^3", poly_c ) ) { return 8; }
	return 0;
}