#include <stdio.h>

// Lab 09 //

void exercise2_4() {
	int a = 4;
	int b = 8;
	printf("a is %d and b is %d.", a, b);
	swap( &a, &b);
	printf("\na is %d and b is %d.", a, b);
}

void swap( int *a, int *b ) {
	int c = *a;
	*a = *b;
	*b = c;
}

void exercise2_5() {
	int a=1, b=3, c=3;
	printf("\na=%d, b=%d, c=%d\n", a, b, c );
	sum( &a, &b, &c );
	printf("\na=%d, b=%d, c=%d\n", a, b, c );
}

void sum( int *a, int *b, int *c ) {
	*c = *a + *b;
}

int main(int argc, char **argv)
{
//	exercise2_4();
	exercise2_5();
	return 0;
}