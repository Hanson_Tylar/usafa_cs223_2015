#include <stdio.h>
#include <ctype.h>
#include <string.h>

int exercise2() {
	char string[50];
	char option[2] = "e";
	printf( "\nA) Return number of vowels and consonants.\n" );
	printf( "B) Convert string to uppercase.\n" );
	printf( "C) Convert string to lowercase.\n" );
	printf( "D) Compare to another string.\n" );
	printf( "E) Enter a new string.\n" );
	printf( "X) Exit the program.\n" );
	while( strcmp( toupper(option), "X") ) {		
		switch( toupper( option[0] ) ) {
			case 'E':
				printf( "\nEnter a string: " );
				fflush( stdin );
				fgets( string, 50, stdin );
				// strlen counts newline character \n but not null \0
				string[strlen(string)-1]='\0';
				int length = strlen( string );
				break;
			case 'A':
				count( string, length );
				break;
			case 'B':
				all_caps( string, length );
				break;
			case 'C':
				all_lower( string, length );
				break;
			case 'D':
				compare( string );
				break;
			case 'X':
				return 0;
		}
		printf( "\nEnter an option: " );
		fflush( stdin );
		scanf( "%1c", option );

	
	}
}

int count( char string[], int length ) {
	int i;
	int vowels = 0;
	int consonants = 0;
	for( i=0; i < length; i++ ) {
		if( isalpha( string[i] ) ) {
			switch( toupper( string[i] ) ) {
				case 'A':
				case 'E':
				case 'I':
				case 'O':
				case 'U':
					vowels ++;
					break;
				default:
					consonants ++;
			}
		}
	}
	printf("\nThere are %d vowels and %d consonants.\n", vowels, consonants);
}

int all_caps( char string[], int length ) {
	int i;
	char result[length];
	for( i=0; i < length; i++ ) {
		result[i] = toupper( string[ i ] );
	}
	printf( "\n%s", result );
}

int all_lower( char string[], int length ) {
	int i;
	char result[length];
	for( i=0; i < length; i++ ) {
		result[i] = tolower( string[ i ] );
	}
	printf( "\n%s", result );
}

int compare( char string[] ) {
	char other[50];
	printf( "\nEnter string to compare: " );
	fflush( stdin );
	fgets( other, 50, stdin );
	other[strlen(other)]='\0';
	int retval = strcmp( string, other );
	if( retval < 0 ) {
		printf( "\n%s comes before %s", string, other );
	}
	else if( retval > 0 ) {
		printf( "\n%s comes after %s", string, other );
	}
	else {
		printf( "\n%s is the same as %s", string, other );
	}
}

int exercise3() {
	printf( "Enter a string to copy: " );
	char string[50];
	fgets( string, 50, stdin );
	char string_copy[50];
	int i =0;
	while( string[i] != '\n' ) {
		string_copy[i] = string[i];
		i++;
	}
	string[i] = '\0';
	printf( "Copied string: %s", string_copy );
}
int main(int argc, char **argv) {
//	exercise2();
	exercise3();
	return 0;
}
