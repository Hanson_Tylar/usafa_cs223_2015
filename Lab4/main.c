#include <stdio.h>

int exercise2() {
	int number;
	srand(time(NULL));
	int rand_num = rand() % 10 + 1;
	int i = 0;
	while( i < 3 && number != rand_num ) {
		printf("Input an integer: ");
		scanf("%d", &number);
		if( number==rand_num ) {
			printf("You guessed correct!\n");
			i = 3;
		}
		else {
			i++;
		}
	}
}

int exercise3() {
	// for( init; condition; increment ) [
	//		statements;
	float value;
	float total = 0;
	int i;
	for( i=0; i < 5; i++ ) {
		printf( "Input a value: " );
		scanf( "%f", &value );
		total += value;
	}
	float avg = total / 5;
	printf( "The average is %2.2f", avg );
}

exercise4() {
	int option = 1;
	while( option !=5 ) {
		printf( "\nChoose an option:\n");
		printf( "1) Option 1\n" );
		printf( "2) Option 2\n" );
		printf( "3) Option 3\n" );
		printf( "4) Option 4\n" );
		printf( "5) Quit\n" );
		int retval = scanf( "%d", &option );
		if( retval !=1 ) {
			printf( "You must input an integer.");
			fflush( stdin );
		}
		printf( "\n" );
		switch( option ) {
			case 1 :
				printf( "You chose Option 1\n" );
				break;
			case 2 :
				printf( "You chose Option 2\n" );
				break;
			case 3 :
				printf( "You chose Option 3\n" );
				break;
			case 4 :
				printf( "You chose Option 4\n" );
				break;
		}
	}
	printf( "You selected quit." );
}
int main(int argc, char **argv)
{
	exercise2();
	exercise3();
	exercise4();
	return 0;
}
