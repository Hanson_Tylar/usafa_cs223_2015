#include "spell_checker.h"

/*
typedef struct tree {
	struct tree *left;
	struct tree *down;
	struct tree *right;
	char letter;
} tree, *TreePtr; */

TreePtr insert(TreePtr root, char *word) {
	if(word[0]=='\0' && root==NULL) {
		TreePtr result = malloc(sizeof(struct tree));
		result->letter = '\0';
		result->left = NULL;
		result->right = NULL;
		result->down = NULL;
		return result;
	}
	else if(root==NULL) {
		TreePtr result = malloc(sizeof(struct tree));
		result->letter = word[0];
		result->left = NULL;
		result->right = NULL;
		result->down = insert(NULL, word+1); 
		return result;
	}
	else if(word[0]<root->letter) {
		root->left = insert(root->left, word);
		return root;
	}
	else if(word[0]>root->letter) {
		root->right = insert(root->right, word);
		return root;
	}
	else {
		root->down = insert(root->down, word+1);
		return root;
	}
}
	
TreePtr insertBalanced(TreePtr root, char *words[], int first, int last) {
	if (first>last){
		return root;
	}
	else {
		int center = (first+last) / 2;
		root = insert(root, words[center]);
		root = insertBalanced(root, words, first, center - 1);
		root = insertBalanced(root, words, center + 1, last);
		return root;
	}
}

TreePtr loadDictionary(FILE *dictionary) {
	// Count how many lines in dictionary
	int num_lines = 0;
	char ch;
	while(!feof(dictionary)) {
		ch = fgetc(dictionary);
		if(ch == '\n') {
			num_lines++;
		}
	}
	fseek(dictionary, 0, SEEK_SET);
	// Make array of pointers to words in dictionary
	int i = 0;
	char *words[num_lines];
	char word[MAX_WORD_LENGTH];
	char *tmp;
	while(!feof(dictionary)) {
		fgets(word, MAX_WORD_LENGTH, dictionary);
		if(word[strlen(word)-1] == '\n') {
			word[strlen(word)-1] = '\0';
		}
		tmp = malloc(strlen(word)+1);
//		strncpy(tmp, word, MAX_WORD_LENGTH);
		// Why is strncpy not working?
		strcpy(tmp, word);
		words[i] = tmp;
		i++;
	}
	// Load array of words into balanced tree
	TreePtr root = NULL;
	root = insertBalanced(root, words, 0, num_lines);
	for(int j=0; j<i; j++) {
		free(words[j]);
	}
	return root;
}

int findWord(TreePtr root, char *word) {
	if(root==NULL) {
		return 0;
	}
	else if(root->letter=='\0' && word[0]=='\0') {
		return 1;
	}
	else if(tolower(word[0])<root->letter) {
		findWord(root->left, word);
	}
	else if(tolower(word[0])>root->letter) {
		findWord(root->right, word);
	}
	else {
		findWord(root->down, word+1);
	}
}


void freeTree(TreePtr root) {
	if(root==NULL) {
		return;
	}
	else {
		freeTree(root->left);
		freeTree(root->right);
		freeTree(root->down);
		free(root);
	}
}