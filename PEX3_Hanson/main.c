#include "spell_checker.h"
#include "TestsHanson.h"

void run_spell_checking(TreePtr root, FILE *test ) {
	// Read lines from the given test file
	int quit = 0;
	int line_num = 0;
	char line[MAX_LINE_LENGTH] = "";
	char line_copy[MAX_LINE_LENGTH] = "";
	char *word;
	int num_words = 0;
	int num_misspelled_words = 0;
	while(!feof(test) && quit != 1) {
		line_num++;
		fgets(line, MAX_LINE_LENGTH, test);
		strcpy(line_copy, line);
		if(line_copy[strlen(line_copy)-1] == '\n') {
			line_copy[strlen(line_copy)-1] = '\0';
		}
		//  Get a word from the line
		word = strtok(line, " ./-_?,!\n");
		while(word != NULL && quit != 1) {
			if(findWord(root, word) == 1) {
				num_words++;
				word = strtok(NULL, " ./-_?,!\n");
			}
			else {
				num_misspelled_words++;
				num_words++;
				printf("\nOn line %d: '%s' ; '%s' is misspelled.\n", line_num, line_copy, word);
				fflush(stdin);
				printf("\nEnter 1 to quit. 2 To add word to dictionary. \nAnything else to go to next misspelled word: ");
				scanf("%d", &quit);
				// Put word into the dictionary
				if(quit==2) {
					insert(root, word);
					quit = 0;
				}
				word = strtok(NULL, " ./-_?,!\n");
			}
		}
		}
	printf("\nNumber of words checked: %d\n", num_words);
	printf("Number of misspelled words: %d\n", num_misspelled_words);
	
}
int main(int argc, char **argv) {
	printf("C3C Tylar Hanson\nPex3 Spell Checker\nDocumentation: None\n\n");
	// make sure user inputs a filename
	if(argc != 2) {
		printf("Usage: %s filename", argv[0]);
		return -1;
	}
	// Load Dictionary
	FILE *dictionary = fopen( "Dictionary.txt", "r+" );
	if ( dictionary == NULL ) {
		printf( "Error opening file!\n" );
		exit(1);
	}
	TreePtr root = loadDictionary(dictionary);
	fclose(dictionary);
	// Run unit tests
	int num = run_unit_tests();
	if(num==0) {
		printf("All unit tests successful.\n\n");
	}
	else {
		printf("Unit test number %d failed.\n\n", num);
	}
	// Open users file
//	FILE *test = fopen("testfile.txt", "r+");
	FILE *test = fopen(argv[1], "r+");
	if ( test == NULL ) {
		printf( "Error opening file!\n" );
		exit(1);
	}
	run_spell_checking(root, test);
	fclose(test);
	freeTree(root);
	return 0;
}
