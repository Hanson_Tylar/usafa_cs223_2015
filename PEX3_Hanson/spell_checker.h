#ifndef SPELL_CHECK
#define SPELL_CHECK

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define MAX_WORD_LENGTH 100
#define MAX_LINE_LENGTH 1000

//Documentation: None

typedef struct tree {
	struct tree *left;
	struct tree *down;
	struct tree *right;
	char letter;
} tree, *TreePtr;

// Load dictionary
TreePtr loadDictionary(FILE *dictionary);

// Insert a word into the search tree
TreePtr insert(TreePtr root, char *word);

// Find a word in the tree, return 1 if found, 0 if otherwise.
int findWord(TreePtr root, char *word);

// free the tree
void freeTree(TreePtr root);

// insert balanced
TreePtr insertBalanced(TreePtr root, char *words[], int first, int last);

// Unit tests
int run_unit_tests();

#endif