#include "TestsHanson.h"

int run_unit_tests() {
	
	TreePtr root =  NULL;
	int a = 0;
	
	//	put a word in then find the word
	root = insert(root, "brown");
	a = findWord(root, "brown");
	if(a!=1) { return 1; }
	// try finding part of that word
	a = findWord(root, "bro");
	if(a==1) { return 2; }
	// try finding a word where original word is a base
	a = findWord(root, "brownies");
	if(a==1) { return 3; }
	
	root = insert(root, "sentence");
	a = findWord(root, "sentence");
	if(a!=1) { return 4; }
	a = findWord(root, "sent");
	if(a==1) { return 5; }
	a = findWord(root, "sentences");
	if(a==1) { return 6; }
	
	root = insert(root, "handle");
	a = findWord(root, "handle");
	if(a!=1) { return 7; }
	a = findWord(root, "hand");
	if(a==1) { return 8; }
	a = findWord(root, "handles");
	if(a==1) { return 9; }
	
	root = insert(root, "formal");
	a = findWord(root, "formal");
	if(a!=1) { return 10; }
	a = findWord(root, "form");
	if(a==1) { return 11; }
	a = findWord(root, "formality");
	if(a==1) { return 12; }
	
	root = insert(root, "punch");
	a = findWord(root, "punch");
	if(a!=1) { return 13; }
	a = findWord(root, "pun");
	if(a==1) { return 14; }
	a = findWord(root, "punched");
	if(a==1) { return 15; }
	
	root = insert(root, "hope");
	a = findWord(root, "hope");
	if(a!=1) { return 16; }
	a = findWord(root, "hop");
	if(a==1) { return 17; }
	a = findWord(root, "hopeful");
	if(a==1) { return 18; }
	
	root = insert(root, "transformation");
	a = findWord(root, "transformation");
	if(a!=1) { return 19; }
	a = findWord(root, "transform");
	if(a==1) { return 20; }
	a = findWord(root, "transformational");
	if(a==1) { return 21; }
	
	root = insert(root, "progress");
	a = findWord(root, "progress");
	if(a!=1) { return 22; }
	a = findWord(root, "pro");
	if(a==1) { return 23; }
	a = findWord(root, "progression");
	if(a==1) { return 24; }
		
	return 0;
	
}