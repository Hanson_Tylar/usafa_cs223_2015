##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=PEX3_Hanson
ConfigurationName      :=Debug
WorkspacePath          := "C:\Users\C18Tylar.Hanson\Documents\Spring_2016\CS_223\CodeLiteWorkspace"
ProjectPath            := "C:\Users\C18Tylar.Hanson\Documents\Spring_2016\CS_223\CodeLiteWorkspace\Pex3_Hanson"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=C18Tylar.Hanson
Date                   :=21/04/2016
CodeLitePath           :="C:\Program Files\CodeLite"
LinkerName             :=C:/TDM-GCC-64/bin/g++.exe
SharedObjectLinkerName :=C:/TDM-GCC-64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="PEX3_Hanson.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/TDM-GCC-64/bin/windres.exe
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/TDM-GCC-64/bin/ar.exe rcu
CXX      := C:/TDM-GCC-64/bin/g++.exe
CC       := C:/TDM-GCC-64/bin/gcc.exe
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := C:/TDM-GCC-64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files\CodeLite
Objects0=$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IntermediateDirectory)/spell_checker.c$(ObjectSuffix) $(IntermediateDirectory)/TestsHanson.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@$(MakeDirCommand) "./Debug"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/main.c$(ObjectSuffix): main.c $(IntermediateDirectory)/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/C18Tylar.Hanson/Documents/Spring_2016/CS_223/CodeLiteWorkspace/Pex3_Hanson/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main.c$(ObjectSuffix) -MF$(IntermediateDirectory)/main.c$(DependSuffix) -MM "main.c"

$(IntermediateDirectory)/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main.c$(PreprocessSuffix) "main.c"

$(IntermediateDirectory)/spell_checker.c$(ObjectSuffix): spell_checker.c $(IntermediateDirectory)/spell_checker.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/C18Tylar.Hanson/Documents/Spring_2016/CS_223/CodeLiteWorkspace/Pex3_Hanson/spell_checker.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/spell_checker.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/spell_checker.c$(DependSuffix): spell_checker.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/spell_checker.c$(ObjectSuffix) -MF$(IntermediateDirectory)/spell_checker.c$(DependSuffix) -MM "spell_checker.c"

$(IntermediateDirectory)/spell_checker.c$(PreprocessSuffix): spell_checker.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/spell_checker.c$(PreprocessSuffix) "spell_checker.c"

$(IntermediateDirectory)/TestsHanson.c$(ObjectSuffix): TestsHanson.c $(IntermediateDirectory)/TestsHanson.c$(DependSuffix)
	$(CC) $(SourceSwitch) "C:/Users/C18Tylar.Hanson/Documents/Spring_2016/CS_223/CodeLiteWorkspace/Pex3_Hanson/TestsHanson.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/TestsHanson.c$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/TestsHanson.c$(DependSuffix): TestsHanson.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/TestsHanson.c$(ObjectSuffix) -MF$(IntermediateDirectory)/TestsHanson.c$(DependSuffix) -MM "TestsHanson.c"

$(IntermediateDirectory)/TestsHanson.c$(PreprocessSuffix): TestsHanson.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/TestsHanson.c$(PreprocessSuffix) "TestsHanson.c"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


