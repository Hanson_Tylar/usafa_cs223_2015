#include <stdio.h>

int exercise2() {
	// to declare an array
	// type arrayName[ arraySize ];
	int numbers[3];
	int i = 0;
	int value;
	int index;
	for( i; i < 3; i++ ) {
		printf("Enter a number: ");
		scanf( "%d", &value );
		numbers[i] = value;
	}
	printf("\nEnter an index number: ");
	scanf( "%d", &index );
	if( index > 0 && index <= i ) {
		printf( "The value at index %d is %d", index, numbers[index - 1] );
	}
	else {
		printf( "Error: The index is invalid." );
	}
	
}

int exercise3() {
	int numbers[3];
	int i;
	int value;
	int index;
	for( i=0; i < 3; i++ ) {
		printf("Enter a number: ");
		scanf( "%d", &value );
		numbers[i] = value;
	}
	printf("\nEnter an index number: ");
	scanf( "%d", &index );
	printf( "The value at index %d is %d", index, numbers[index - 1] );
}

int exercise4() {
	int array[4][5] = {
		{ 1, 2, 3, 4, 5 } ,
		{ 2, 4, 6, 8, 10 } ,
		{ 20, 10, 5, 3, 1 } ,
		{ 3, 6, 9, 12, 15 }
	};
	
	int i;
	int j;
	int sum = 0;
	int row_sums[4];
	for( i=0; i < 4; i++ ) {
		for( j=0; j < 5; j++ ) {
			sum += array[i][j];
		}
		row_sums[i] = sum;
		sum = 0;
	}
	
	int col_sums[5];
	for( j=0; j < 5; j++ ) {
		for( i=0; i < 4; i++ ) {
			sum += array[i][j];
		}
		col_sums[j] = sum;
		sum = 0;
	}
	
	int grand_total = 0;
	for( i=0; i < 4; i++ ) {
		grand_total += row_sums[i] + col_sums[i];
	}
	grand_total += col_sums[5];
	
	int new_array[] = {
		  1, 2, 3, 4, 5, row_sums[0],
		  2, 4, 6, 8, 10, row_sums[1],
		  20, 10, 5, 3, 1, row_sums[2],
		  3, 6, 9, 12, 15, row_sums[3],
		  col_sums[0], col_sums[1], col_sums[2], col_sums[3], col_sums[4], grand_total
	};
	for( i=0; i<5*6; i++ ) {
		if( i % 6 == 0 ){
			printf( "\n" );
		}
		printf( " %3.0d ", new_array[i] );
	}
}

int main(int argc, char **argv)
{
//	exercise2();
//	exercise3();
	exercise4();
	return 0;
}
