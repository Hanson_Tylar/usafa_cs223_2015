#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

/*
typedef struct tree {
	int item;
	struct tree *left;
	struct tree *right;
} Tree, *TreePtr;       */

// insert a number into the binary search tree in order
TreePtr insert(TreePtr root, int item) {
	if(root==NULL) {
		TreePtr result = malloc(sizeof(struct tree));
		result->item = item;
		result->left = NULL;
		result->right = NULL;
		return result;
	}
	else if(item<root->item) {
		root->left = insert(root->left, item);
		return root;
	}
	else {
		root->right = insert(root->right, item);
		return root;
	}
}

// return 1 if item is in the tree, 0 if otherwise
int find(TreePtr root, int item) {
	int result;
	if(root==NULL) {
		return 0;
	}
	else if(item<root->item) {
		result = find(root->left, item);
	}
	else if(item>root->item) {
		result = find(root->right, item);
	}
	else {
		result = 1;
	}
	return result;
}

// free the tree
void freeTree(TreePtr root) {
	if(root==NULL) {
		return;
	}
	else {
		freeTree(root->left);
		freeTree(root->right);
		free(root);
	}
}

// print all the #s in the tree in increasing order
// In-order traversal
void printInOrder(TreePtr root) {
	if(root==NULL) {
		return;
	}
	else {
		printInOrder(root->left);
		printf("%d\n", root->item);
		printInOrder(root->right);
	}
}

TreePtr insertBalanced(TreePtr root, int nums[],int firstIndex, int lastIndex){
	if (firstIndex > lastIndex){
		return root;
	}
	else {
		int middleguy = (firstIndex+lastIndex) / 2;
		root = insert(root, nums[middleguy]);
		root = insertBalanced(root, nums, firstIndex, middleguy - 1);
		root = insertBalanced(root, nums, middleguy + 1, lastIndex);
		return root;
	}
}

// find the smallest # in the tree
int findSmallest(TreePtr root){
	if (root->left == NULL){
		return root->item;
	}
	else{
		return findSmallest(root->left);
	}
	
}

//deletes something and rearranges the tree
TreePtr deletetree(TreePtr root, int item){
	if (root == NULL){
		return root;
	}
	else if (item>root->item){
		root->right = deletetree(root->right, item);
		return root;
	}
	else if (item<root->item){
		root->left = deletetree(root->left, item);
		return root;
	}
	else if (root->left == NULL && root->right == NULL){
		free(root);
		return NULL;
	}
	else if (root->right == NULL){
		TreePtr left_tree = root->left;
		free(root);
		return left_tree;
	}
	else{
		int smallest = findSmallest(root->right);
		root->right = deletetree(root->right, smallest);
		root->item = smallest
		return root;
	}
}