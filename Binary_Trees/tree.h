typedef struct tree {
	int item;
	struct tree *left;
	struct tree *right;
} Tree, *TreePtr;

// insert a number into the binary search tree in order
TreePtr insert(TreePtr root, int item);

// return 1 if item is in the tree, 0 if otherwise
int find(TreePtr root, int item);

// free the tree
void freeTree(TreePtr root);

// print all the #s in the tree in increasing order
// In-order traversal
void printInOrder(TreePtr root);