#include <stdio.h>
#include <stdlib.h>

// Lab 19 
typedef struct node {
	int digit;
	struct node *next;
} num, *intPtr;

intPtr add_nums( intPtr a, intPtr b, int carry ) {
	intPtr result;
	if( a == NULL && b == NULL ) {
		if( carry > 0 ) {
		result = malloc(sizeof( struct node));
		result -> digit =  carry;
		result -> next = NULL;
		return result;
		}
	else{
		return NULL;
	}
	}
	else if( a == NULL ) {
		result = malloc(sizeof( struct node));
		result -> digit = ( b -> digit + carry ) % 10 ;
		carry = ( b -> digit + carry ) / 10;
		result -> next = add_nums( a, b -> next, carry );
	}
	else if( b == NULL ) {
		result = malloc(sizeof(struct node));
		result -> digit = ( a -> digit + carry ) % 10;
		carry = ( a -> digit + carry ) / 10;
		result -> next = add_nums( a -> next, b, carry );
	}
	else {
		result = malloc(sizeof( struct node));
		result -> digit = ( a -> digit + b -> digit + carry ) % 10;
		carry = ( a -> digit + b -> digit + carry ) / 10;
		result -> next = add_nums( a -> next, b -> next, carry );
	}
	return result;
}
	
intPtr insertItem( intPtr head, int item ) {
	intPtr temp;
	// I can solve one small problem
	// Zero items in the list
	if( head==NULL ) {
		temp = malloc(sizeof(struct node));
		temp -> digit = item;
		temp -> next = NULL;
		return temp;
	}
	// Inserting before the first item in the list
	else {
		temp = malloc(sizeof(struct node));
		temp -> digit = item;
		temp -> next = head;
		return temp;
	}
}

void printList( intPtr head ) {
	if( head == NULL ) {
		return;
	}
	else {
		printf( "%d\n", head -> digit );
		printList( head -> next );
	}
}

int main(int argc, char **argv){
	intPtr number1=NULL;
	intPtr number2=NULL;
	intPtr result;
	number1 = insertItem( number1, 9 );
	number1 = insertItem( number1, 9 );
	number1 = insertItem( number1, 9 );
	number1 = insertItem( number1, 8 );
	number2 = insertItem( number2, 2 );
//	number2 = insertItem( number2, 0 );
//	number2 = insertItem( number2, 0 );
//	number2 = insertItem( number2, 0 );
	result = add_nums( number1, number2, 0 );
	printList( result );
	return 0;
}