#include <stdio.h>

// Lab 08 //

void exercise2_1() {
	printf("Arguments passed by value can be variables (e.g. x), literals (e.g. 6), expressions (e.g. x+1), structs & classes, and enumerators.\nArguments are never changed by the function being called, which prevents side effects.");
}
int main(int argc, char **argv)
{
	exercise2_1();
//	exercise2_2();
//	exercise2_3();
	return 0;
}
