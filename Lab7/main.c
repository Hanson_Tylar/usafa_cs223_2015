#include <stdio.h>
#include <string.h>

// LAB 07 //

struct Date {
	int month;
	int day;
	int year;
};

int whos_older( struct Date d1, struct Date d2 ) {
	if(d1.year<d2.year) {
		return -1;
	}
	else if(d1.year>d2.year) {
		return 1;
	}
	else if(d1.month<d2.month) {
		return -1;
	}
	else if(d1.month>d2.month) {
		return 1;
	}
	else if(d1.day<d2.day) {
		return -1;
	}
	else if(d1.day>d2.day) {
		return 1;
	}
	else {
		return 0;
	}
}
int main(int argc, char **argv)
{
	int date1month, date1day, date1year ;
	int date2month, date2day, date2year ;
	
	printf( "Enter date 1 month: ");
	scanf( "%d", &date1month );
	printf( "Enter date 1 day: ");
	scanf( "%d", &date1day );
	printf( "Enter date 1 year: ");
	scanf( "%d", &date1year );
	
	printf( "\nEnter date 2 month: ");
	scanf( "%d", &date2month );
	printf( "Enter date 2 day: ");
	scanf( "%d", &date2day );
	printf( "Enter date 2 year: ");
	scanf( "%d", &date2year );
	
	struct Date d1;
	d1.month = date1month;
	d1.day = date1day;
	d1.year = date1year;
	struct Date d2;
	d2.month = date2month;
	d2.day = date2day;
	d2.year = date2year;
	
	fflush( stdin );
	if( whos_older( d1, d2 ) > 0 ) {
		printf( "\n%d.%d.%d comes after %d.%d.%d\n", d1.month, d1.day, d1.year, d2.month, d2.day, d2.year );
	}
	else if( whos_older( d1, d2 ) < 0 ) {
		printf( "\n%d.%d.%d comes before %d.%d.%d\n", d1.month, d1.day, d1.year, d2.month, d2.day, d2.year );
	}
	else {
		printf( "\n%d.%d.%d is the same day as %d.%d.%d", d1.month, d1.day, d1.year, d2.month, d2.day, d2.year );
	}
	
	return 0;
}
