#ifndef PriorityQueue
#define PriorityQueue
#include "Graph.h"

typedef struct queueNode {
	int node;
	int pred;
	int distance;
	struct queueNode *next;
} *QueueNodePtr;

//enqueue
QueueNodePtr enqueue(QueueNodePtr Q, int vertex, int dist, int predecessor);

//dequeue
QueueNodePtr dequeue(QueueNodePtr Q, int *vertex, int *dist, int *predecessor);

// empty, 1 if empy 
int isEmpty(QueueNodePtr Q);

// Dijkstra's Algorithm
// pass arrays of distance and predesseccors
void dijkstrasAlgorithm(Board *B, int *distances, int *preds);

#endif