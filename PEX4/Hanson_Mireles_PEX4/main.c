#undef main //eliminates duplicate main within SDL libraries
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "InstructorGameLogic.h"

/*These two functions need to be modified */
int executeTurn(int turn, SDL_Renderer *ren, SDL_MouseButtonEvent e, Board *B, int mode, int option, int *distances, int* preds);
int determineWinner(Board *B, int *distances, int *preds);
/* You'll want to add a method for computerPlayerShort */
int executeComputerShort(SDL_Renderer *ren, Board *B, int *distances, int *preds);
int executeComputerCut(SDL_Renderer *ren, Board *B, int *distances, int *preds);

/* The main method may need to be modified to add the selection of Player vs Player or Computer vs Player -- see below */

int main(int argc, char **argv) {
	printf("C3C Tylar Hanson, Lucas Mireles\nPex4 Shannon Switching Game\nDocumentation: None\n\n");
	SDL_Window *win = NULL;
	SDL_Renderer *ren = NULL;
	SDL_Event e;
	Board B;
	B.num = numNodes;
	
	int distances[numNodes];
	int preds[numNodes];

	int quit = GAME_UNDERWAY; 
	srand(time(NULL));
	int choice = rand() % 2;
	int turn = CUT_TURN; 
	if(choice==1){
		turn = SHORT_TURN;
	}

	// TODO!!!
	// Add a simple menu here that shows up in the black console window to choose PvP or Computer vs. Player
	int option;
	int mode;
	printf("Select a gameplay option\n");
	printf("1) Player vs. Player\n");
	printf("2) Player vs. Computer\n");
	printf("3) Computer vs. Computer\n");
	scanf("%d", &mode);
	if(mode==2) {
		printf("Who would you like to play as?\n");
		printf("1) Short\n2) Cut\n");
		scanf("%d", &option);
	}

	// Probably don't need to modify anything else below

	initializeGraphicsWindow(&win, &ren);
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255); // creates a white background for the board
	SDL_RenderClear(ren); //clears the game board to ensure it is plain white
	SDL_RenderPresent(ren); //renders the gameboard to the screen
	
	if (turn){ stringColor(ren, 5, 5, "Short's Turn: Click to advance Computer's turn ", black); } //displays initial turn
	else{ stringColor(ren, 5, 5, "Cut's Turn: Click to advance Computer's turn ", black); }
   
	createAndDrawBoard(ren,&B);//generates random planar graph and draws it
	
	// Put vertex numbers on the graph
	char num_string[100];
	for(int vertex=0; vertex < numNodes; vertex++ ) {
		sprintf(num_string,"%d",vertex);
		stringColor(ren, B.Vertices[vertex].x+5, B.Vertices[vertex].y+5, num_string, red);
	}
	
	//This is the main loop. 
	//It calls executeTurn once per mouse click until the user quits or someone wins
	while (quit==GAME_UNDERWAY){ 
		while (SDL_PollEvent(&e) != 0) //Loops through event queue
		{
			//User requests quit
			if (e.type == SDL_QUIT) //allows user to x out of program
			{
				quit = USER_QUIT;
			}
			if (e.type == SDL_MOUSEBUTTONDOWN) //handles mouse button events
			{
				if (quit == GAME_UNDERWAY){
					turn = executeTurn(turn, ren, e.button, &B, mode, option, distances, preds);
				}
				quit = determineWinner(&B, distances, preds);  //returns 0,2,or 3 as defined above inline with the declaration of quit

			}
		}
		SDL_RenderPresent(ren); //presents changes to the screen that result from the execution of one turn
	}

	// Display who won
	displayWinBanner(ren, quit);

	while ((quit == CUT_WINS) || (quit == SHORT_WINS))  //if there was a winner hold the screen until the game window is closed.
	{
		SDL_RenderPresent(ren); //present changes to the screen

		// wait until the user closes the window
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = USER_QUIT;
			}
		}
	}

	freeGraphicsWindow(&win, &ren);//Terminate SDL and end program
	deleteBoard(&B); //deallocates dynamic memory
	return 0;
}


/*****************************************************************************************************************************/
/********MODIFY THE FOLLOWING TWO FUNCTIONS TO FINISH THE GAME                                                         *******/
/*****************************************************************************************************************************/
/*
//these functions should use your graph library to model the logical game. 
*/

// This method checks to see if someone has won
// Return CUT_WINS, SHORT_WINS, or GAME_UNDERWAY (if noone has won yet)
int determineWinner(Board *B, int *distances, int *preds) {
	dijkstrasAlgorithm(B, distances, preds);
	if (distances[numNodes-1]>=100){
		return CUT_WINS;
	}
	else if(distances[numNodes-1]==0){
		return SHORT_WINS;
	}
	else{
		return GAME_UNDERWAY;
	}
}

/*
Simple decision logic to handle each SDL event depending on who's turn it is
Returns CUT_TURN or SHORT_TURN depending on who will go next
*/
int executeTurn(int turn, SDL_Renderer *ren, SDL_MouseButtonEvent e, Board *B, int mode, int option, int *distances, int* preds){
	dijkstrasAlgorithm(B, distances, preds);
	// If player vs. player
	if (mode==1){
		if (turn){
			return executePlayerShort(ren, e, B);
		}
		else{
			return executePlayerCut(ren, e, B);
		}
	}
	else if(mode==2){
	// Player vs. computer
		// If Short vs computer
		if(option==1) {
			if (turn) {
				return executePlayerShort(ren, e, B);
			}
			else {
				return executeComputerCut(ren, B, distances, preds);
			}
		}
		// Else Computer vs Cut
		else {
			if (turn){
				return executeComputerShort(ren, B, distances, preds);
			}
			else{
				return executePlayerCut(ren, e, B);
			}
		}
	}
	// Else it is Computer vs. Computer
	else {
		if(turn) {
			return executeComputerShort(ren, B, distances, preds);
		}
		else {
			return executeComputerCut(ren, B, distances, preds);
		}
	}
}

/*****************************************************************************************************************************/
/********ADD YOUR OWN NEW METHODS HERE                                                                                 *******/
/*****************************************************************************************************************************/

int executeComputerShort(SDL_Renderer *ren, Board *B, int *distances, int* preds){
	int i=numNodes-1;
	int j = preds[i];
	while(B->edges[i][j]==lockedArc) {
		i = j;
		j = preds[i];
	}
	B->edges[i][j] = lockedArc;
	B->edges[j][i] = lockedArc;			
	lockArc(ren, B->Vertices[findIndex(B, i)].x, B->Vertices[findIndex(B, i)].y, B->Vertices[findIndex(B, j)].x, B->Vertices[findIndex(B, j)].y);
	if (B->Vertices[findIndex(B, i)].locked>0){
		endNode(ren, B->Vertices[findIndex(B, i)].x, B->Vertices[findIndex(B, i)].y);
	}
	else{ drawNode(ren, B->Vertices[findIndex(B, i)].x, B->Vertices[findIndex(B, i)].y); }
	if (B->Vertices[findIndex(B, j)].locked>0){
		endNode(ren, B->Vertices[findIndex(B, j)].x, B->Vertices[findIndex(B, j)].y);
	}
	else{ drawNode(ren, B->Vertices[findIndex(B, j)].x, B->Vertices[findIndex(B, j)].y); }
	
	stringColor(ren, 5, 5, "Short's Turn: Click to advance Computer's turn ", white);
	stringColor(ren, 5, 5, "Cut's Turn: Click to advance Computer's turn ", black);
	return CUT_TURN;
}

int executeComputerCut(SDL_Renderer *ren, Board *B, int *distances, int* preds){
	int i=numNodes-1;
	int j = preds[i];
	while(B->edges[i][j]==lockedArc) {
		i = j;
		j = preds[i];
	}
	B->edges[i][j] = noArc;
	B->edges[j][i] = noArc;			
	
	eraseArc(ren, B->Vertices[findIndex(B, i)].x, B->Vertices[findIndex(B, i)].y, B->Vertices[findIndex(B, j)].x, B->Vertices[findIndex(B, j)].y);
	if (B->Vertices[findIndex(B, i)].locked>0){
		endNode(ren, B->Vertices[findIndex(B, i)].x, B->Vertices[findIndex(B, i)].y);
	}
	else{ drawNode(ren, B->Vertices[findIndex(B, i)].x, B->Vertices[findIndex(B, i)].y); }
	if (B->Vertices[findIndex(B, j)].locked>0){
		endNode(ren, B->Vertices[findIndex(B, j)].x, B->Vertices[findIndex(B, j)].y);
	}
	else{ drawNode(ren, B->Vertices[findIndex(B, j)].x, B->Vertices[findIndex(B, j)].y); }
	
	stringColor(ren, 5, 5, "Cut's Turn: Click to advance Computer's turn", white);
	stringColor(ren, 5, 5, "Short's Turn: Click to advance Computer's turn", black);
	return SHORT_TURN;
}