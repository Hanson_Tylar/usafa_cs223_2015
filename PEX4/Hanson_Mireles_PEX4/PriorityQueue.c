#include <stdlib.h>
#include "PriorityQueue.h"
#include "Graph.h"

/*typedef struct queueNode {
	int node;
	int pred;
	int distance;
	struct queueNode *next;
} *QueueNodePtr;*/

//enqueue with distance being the priority
QueueNodePtr enqueue(QueueNodePtr Q, int vertex, int dist, int predecessor) {
	QueueNodePtr tmp;
	if(Q==NULL) {
		tmp = malloc(sizeof(struct queueNode));
		tmp->node = vertex;
		tmp->pred = predecessor;
		tmp->distance = dist;
		tmp->next = NULL;
		return tmp;
	}
	else if(dist <= Q->distance) {
		tmp = malloc(sizeof(struct queueNode));
		tmp->node = vertex;
		tmp->pred = predecessor;
		tmp->distance = dist;
		tmp->next = Q;
		return tmp;
	}
	else {
		Q->next = enqueue(Q->next, vertex, dist, predecessor);
		return Q;
	}
}

//dequeue
QueueNodePtr dequeue(QueueNodePtr Q, int *vertex, int *dist, int *predecessor) {
	QueueNodePtr tmp = Q->next;
	*vertex = Q->node;
	*dist = Q->distance;
	*predecessor = Q->pred;
	free(Q);
	return tmp;
}

// empty, 1 if empy 
int isEmpty(QueueNodePtr Q) {
	if(Q==NULL) {
		return 1;
	}
	else {
		return 0;
	}
}

void dijkstrasAlgorithm(Board *B, int *distances, int *preds) {
	QueueNodePtr Q=NULL;
	Q = enqueue(Q, 0, 0, -1);
	int visited[B->num];
	for( int i=0; i<B->num; i++) {
		distances[i] = noArc;
		visited[i] = 0;
		preds[i] = -1;
	}
	while(!isEmpty(Q)) {
		int v; int pred; int dist;
		Q = dequeue(Q, &v, &dist, &pred);
		if(!visited[v]) {
			visited[v] = 1; distances[v] = dist; preds[v] = pred;
			for(int w=0; w< B->num; w++) {
				if(!visited[w] && B->edges[v][w] != noArc) {
					Q = enqueue(Q, w, distances[v] + B->edges[v][w], v);
				}
			}
		}
	}
}